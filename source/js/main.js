//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js

//= ../node_modules/popper.js/dist/umd/popper.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js
//= library/flipclock.js
//= library/slick.js
//= library/rellax.js
//= library/wow.js


$(document).ready(function () {
	var value = 0;
	initCalc();
	/* calc */

	$(".btn_calc").on("click", function (e) {
		e.preventDefault()
		e.stopPropagation();



		$(this).toggleClass("active");

		if ( !($(this).data("id") == "") ) {
			if( $(this).hasClass("active") ) {
				$(".calc__preview").append( "<img class='over img-fluid img__"+ $(this).data("id") +"' src="+ $(this).data("img-src") +" alt='img'>" );
			} else {
				var id = ".img__"+$(this).data("id");
				$(".calc__preview").find(id).remove();

				//$(".calc__preview").append( "<img class='over img__"+ $(this).data("id") +"' src="+ $(this).data("img-src") +" alt='img'>" );
			}
		}

		if( $(this).hasClass("active") ) {
			sumCalc( $(this).data("price"), "+" );
		} else {
			sumCalc( $(this).data("price"),"-" );
		}
	});
	function initCalc() {
		var items = $(".btn_calc.active");

		for( var i = 0; i < items.length; i++ ){
			value += $(items[i]).data("price");	
		}

		$(".calc__price__value").html(value + " ");
	}
	function sumCalc(price, operand) {
		if( operand == "+" ) {
			value += price;
		} else if ( operand == "-" ) {
			value -= price;
		} else {
			console.log("хуевый операнд");
		}
		$( ".calc__price__value" ).html( value + " " );
	}

	/* анимация блоков */
	new WOW().init({
		mobile: false
	});

	/* параллакс */
	var rellax = new Rellax('.rellax', {
	    center: false,
	    round: true,
	    vertical: true,
	    horizontal: true
	  });

	/* блики кнопок */
	setInterval(function () {
		$(".btn__flare_infinite").toggleClass("active");
	}, 2000);

	/* Добавление меню BG при скролле */

	if ( $(window).scrollTop() > 0 ) {
        $("header").addClass("header__color");
    }
	$(window).on("scroll", function () {
		if ( $(window).scrollTop() > 0 ) {
			$("header").addClass("header__color");
		}  else {
			$("header").removeClass("header__color");
		}

		/* кнопка вверх */
		if ( $(window).scrollTop() > 100 ) {
			$(".btn_to_top").addClass("active");
		} else {
			$(".btn_to_top").removeClass("active");
		}
	});
	$('.btn_to_top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });

	/* Slider */
	$('.works').slick({
	  infinite: false,
	  speed: 300,
	  slidesToShow: 3,
	  adaptiveHeight: true,
	  slidesToScroll: 3,
	  nextArrow: '<div class="sldier_arrow_next"><img src="img/Arrow.svg" alt="arrow"></div>',
	  prevArrow: '<div class="sldier_arrow_prev"><img src="img/Arrow.svg" alt="arrow"></div>',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 981,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	}).on('setPosition', function (event, slick) {
    	//slick.$slides.css('height', slick.$slideTrack.height() + 'px');
	});

});
